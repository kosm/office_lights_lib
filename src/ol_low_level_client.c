/*
 * ol_low_level.c
 *
 *  Created on: 20.02.2011
 *      Author: kosm
 */
#include <debug.h>
#include <ol_low_level_client.h>

static OLContext *ol_ctx = NULL;

void ol_ll_client_init(OLContext *ctx) {
	ol_ctx = ctx;
}

void ol_ll_client_dispose() {
	ol_ctx = NULL;
}

int ol_ll_set_entity_params(int entity, const LowLevelReportParam *request_params, size_t param_count) {
	uint8_t olcp_report_buffer[MAX_OLCP_REPORT_SIZE];
	LowLevelReportHead *head = (LowLevelReportHead *)olcp_report_buffer;
	LowLevelReportParam *params = (LowLevelReportParam *)(olcp_report_buffer + sizeof(LowLevelReportHead));
	size_t par_num;

	if (ol_ctx == NULL) {
		debug_output(DEBUG_ERROR, "The client context wasn't initialized\n");
		return OLCP_BSER_RUNTIME;
	}
	if (param_count >= MAX_OLCP_LL_PARAM_COUNT) {
		debug_output(DEBUG_ERROR, "Maximum number of parameters exceeded\n");
		return OLCP_BSER_RUNTIME;
	}

	head->head.report_class = OLCP_RC_LOW_LEVEL;
	head->head.report_code = OLCP_LLC_SET;
	head->entity = (uint8_t)entity;

	for (par_num = 0; par_num < param_count; ++par_num) {
		params[par_num].parameter = request_params[par_num].parameter;
		params[par_num].value = request_params[par_num].value;
	}

	if (!ol_ctx->report_sender(ol_ctx, olcp_report_buffer, sizeof(LowLevelReportHead) +
			sizeof(LowLevelReportParam) * param_count))
		return OLCP_BSER_RUNTIME;

	ol_ctx->report_receiver(ol_ctx);

	return OLCP_BS_NONE;
}
