/*
 * debug.c
 *
 *  Created on: 20.02.2011
 *      Author: kosm
 */

#include <debug.h>

#include <stdlib.h>
#include <stdarg.h>

#define DEBUG_FILE_ENV_NAME "OL_DEBUG_FILE"

static FILE *out_file;
static int dbg_level;

void debug_init(int debug_level) {
	const char *debug_file_name = getenv(DEBUG_FILE_ENV_NAME);

	if (debug_file_name != NULL) {
		if ((out_file = fopen(debug_file_name, "a")) == NULL) {
			perror("fopen()");
			fprintf(stderr, "Using stdout for debug output\n");
			out_file = stdout;
		}
	}
	else out_file = stdout;
	dbg_level = debug_level;
}

void debug_dispose() {
	if (out_file != stdout && fclose(out_file)) perror("fclose()");
}

void debug_set_level(int debug_level) {
	dbg_level = debug_level;
}

void debug_output(int debug_level, const char *format, ...) {
	va_list varg;
	va_start(varg, format);

	if (debug_level >= dbg_level) {
		vfprintf(out_file, format, varg);
	}

	va_end(varg);
}

void debug_hex_dump(int debug_level, const void *data, size_t size) {
	if (debug_level >= dbg_level) {
		size_t byte;
		fprintf(out_file, "[");
		for (byte = 0; byte < size; ++byte) fprintf(out_file, "%02hhX ", ((const char *)data)[byte]);
		fprintf(out_file, "\b], total %d bytes\n", (int)size);
	}
}
