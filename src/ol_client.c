/*
 * ol_client.c
 *
 *  Created on: 20.02.2011
 *      Author: kosm
 */

#include <debug.h>
#include <ol_client.h>
#include <ol_low_level_client.h>

#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>

#define DEFAULT_DEBUG_LEVEL DEBUG_ERROR

#define ERROR_RETURN(func_name) \
	perror(func_name); \
	free(ctx); \
	return NULL;

static bool sender(OLContext *ol_ctx, const void *report, uint8_t size) {
	debug_output(DEBUG_TRACE, "About to send the data: ");
	debug_hex_dump(DEBUG_TRACE, report, size);
	if (write(ol_ctx->device_fd, report, (size_t)size) < 0) {
		perror("write()");
		return false;
	}
	return true;
}

static int receiver(OLContext *ol_ctx) {
	int read_size = 0;

	if ((read_size = read(ol_ctx->device_fd, ol_ctx->report_buffer, sizeof(ol_ctx->report_buffer))) < 0) {
		perror("read()");
		return -1;
	}
	debug_output(DEBUG_TRACE, "Received the data: ");
	debug_hex_dump(DEBUG_TRACE, ol_ctx->report_buffer, read_size);

	return read_size;
}

OLContext *ol_open(const char *device) {
	OLContext *ctx = (OLContext *)malloc(sizeof(OLContext));

	if (ctx == NULL) {
		perror("malloc()");
		return NULL;
	}

	debug_init(DEFAULT_DEBUG_LEVEL);

	if ((ctx->device_fd = open(device, O_RDWR)) < 0) {
		ERROR_RETURN("open()");
	}

	ctx->report_sender = sender;
	ctx->report_receiver = receiver;

	ol_ll_client_init(ctx);

	return ctx;
}

void ol_close(OLContext *ol_ctx) {
	ol_ll_client_dispose();
	if (close(ol_ctx->device_fd) < 0) perror("close()");
	debug_dispose();
	free(ol_ctx);
}
