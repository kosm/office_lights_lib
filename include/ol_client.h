/*
 * ol_client.h
 *
 *  Created on: 20.02.2011
 *      Author: kosm
 */

#ifndef OL_CLIENT_H_
#define OL_CLIENT_H_

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "olcp.h"

struct _OLContext;

typedef bool (* ReportSender)(struct _OLContext *ol_ctx, const void *report, uint8_t size);
typedef int (* ReportReceiver)(struct _OLContext *ol_ctx);

typedef struct _OLContext {
	int device_fd;
	ReportSender report_sender;
	ReportReceiver report_receiver;
	uint8_t report_buffer[MAX_OLCP_REPORT_SIZE];
} OLContext;

OLContext *ol_open(const char *device);
void ol_close(OLContext *ol_ctx);

#endif /* OL_CLIENT_H_ */
