/*
 * proto.h
 *
 *  Created on: 17.02.2011
 *      Author: kosm
 */

#ifndef PROTO_H_
#define PROTO_H_

#include <stdbool.h>
#include <stdint.h>

/* The basic report class */
#define OLCP_RC_BASIC 0x00
/* The low level report class */
#define OLCP_RC_LOW_LEVEL 0x01
/* The high level report class */
#define OLCP_RC_HIGH_LEVEL 0x02

/* The report codes of the basic report class */
#define OLCP_BS_ACK 0x00 /* An acknowledge report */
#define OLCP_BS_NONE 0x01 /* A NULL-report */
#define OLCP_BS_ERROR 0xFF /* An error report */

/* The basic error reason codes */
#define OLCP_BSER_NOERROR 0x00
#define OLCP_BSER_INVALID_VALUE 0x01 /* An invalid value has been passed */
#define OLCP_BSER_HARDWARE 0x02 /* A hardware error */
#define OLCP_BSER_RUNTIME 0x03 /* A runtime error */
#define OLCP_BSER_INVALID_CLASS 0x04 /* An invalid report class */
#define OLCP_BSER_UNKNOWN 0xFF /* A very strange unknown error ... */

#define MAX_OLCP_REPORT_SIZE 16

/* Each OLCP response has the following format:
 * <RESPONSE_CODE> [<VALUE>]
 */

/* The request codes */

typedef struct {
	uint8_t report_class;
	uint8_t report_code;
} BasicReport;

typedef struct {
	BasicReport head;
	uint8_t reason;
} BasicErrorResponse;

#endif /* PROTO_H_ */

