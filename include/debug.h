/*
 * debug.h
 *
 *  Created on: 20.02.2011
 *      Author: kosm
 */

#ifndef DEBUG_H_
#define DEBUG_H_

#include "ol_client.h"

#include <stdio.h>

#define DEBUG_TRACE 0x00
#define DEBUG_ERROR 0x01
#define DEBUG_NONE 0xFF

void debug_init(int debug_level);
void debug_dispose();
void debug_set_level(int debug_level);
void debug_output(int debug_level, const char *format, ...);
void debug_hex_dump(int debug_level, const void *data, size_t size);

#endif /* DEBUG_H_ */
