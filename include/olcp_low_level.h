/*
 * olcp_low_level.h
 *
 *  Created on: 19.02.2011
 *      Author: kosm
 */

#ifndef OLCP_LOW_LEVEL_H_
#define OLCP_LOW_LEVEL_H_

#include "olcp.h"

/* The low level entities */

/* Each OLCP request has the following format:
 * <REPORT_CLASS> <REQUEST_CODE> <ENTITY> <PARAM> <VALUE> [ <PARAM> <VALUE> ... ]
 */

/* The request codes of the low level report class */
#define OLCP_LLC_GET 0x00 /* The request for a parameter value */
#define OLCP_LLC_SET 0x01 /* The request for change a parameter */

/* The supported low level entities */
#define OLCP_LLE_POWERMON 0x01

#define OLCP_LLE_CHANNEL1 0x10
#define OLCP_LLE_CHANNEL2 0x11
#define OLCP_LLE_CHANNEL3 0x12
#define OLCP_LLE_CHANNEL4 0x13
#define OLCP_LLE_CHANNEL5 0x14
#define OLCP_LLE_CHANNEL6 0x15
#define OLCP_LLE_CHANNEL7 0x16

/* The power monitor params */
#define OLCP_LLP_POWERMON_OUTPUT_POWER 0x01

/* The channel params */
#define OLCP_LLP_CHANNEL_STATE 0x00
#define OLCP_LLP_CHANNEL_BRIGHTNESS 0x01
#define OLCP_LLP_CHANNEL_CHANGE_PERIOD 0x02
#define OLCP_LLP_CHANNEL_BLINKING 0x03
#define OLCP_LLP_CHANNEL_BLINKING_PERIOD 0x04

/* The generic values */
#define OLCP_LLV_DISABLED 0x00
#define OLCP_LLV_ENABLED 0x01

/* The low level errors */
#define OLCP_LLER_UNKNOWN_ENTITY 0x00
#define OLCP_LLER_UNKNOWN_PARAM 0x01

#define MAX_OLCP_LL_PARAM_COUNT 6

typedef struct {
	BasicReport head;
	uint8_t entity;
} LowLevelReportHead;

typedef struct {
	uint8_t parameter;
	uint8_t value;
} LowLevelReportParam;

bool process_ll_request(const void *request, const uint8_t length, uint8_t *result_code);
bool build_ll_request(void *request_buffer, const uint8_t buffer_length, uint8_t *result_code);

bool process_ll_response(const LowLevelReportHead *request, uint8_t *result_code);
bool build_ll_response(const LowLevelReportHead *request, uint8_t *result_code);

#endif /* OLCP_LOW_LEVEL_H_ */
