/*
 * ol_low_level_client.h
 *
 *  Created on: 20.02.2011
 *      Author: kosm
 */

#ifndef OL_LOW_LEVEL_CLIENT_H_
#define OL_LOW_LEVEL_CLIENT_H_

#include "ol_client.h"
#include "olcp_low_level.h"

void ol_ll_client_init(OLContext *ctx);
void ol_ll_client_dispose();

void ol_ll_enable_channel(int channel_number);
void ol_ll_disable_channel(int channel_number);
void ol_ll_set_channel_state(int channel_number, bool state);
int ol_ll_set_entity_params(int entity, const LowLevelReportParam *request_params, size_t param_count);

#endif /* OL_LOW_LEVEL_CLIENT_H_ */
